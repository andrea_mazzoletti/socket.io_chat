var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});


io.on('connection', function (socket) {

    console.log('--- a user connected ---');
    io.emit('connected', '--- a user just connected ---');

    socket.on('disconnect', function () {
        console.log('--- a user disconnected ---');
        io.emit('disconnect', '--- a user just disconnected ---');
    });

    socket.on('chat message', function (msg) {
        console.log('message: ' + msg);
        io.emit('chat message', msg);
    });
});

http.listen(3000, function () {
    console.log('listening on *:3000');
});